<?php

require 'animal.php';
require 'ape.php';
require 'frog.php';

$sheep = new Animal("shaun");

echo "<h3> SOAL 1 </h3>";
echo $sheep->name; // "shaun"
echo "<br>";
echo $sheep->legs; // 2
echo "<br>";
echo $sheep->cold_blooded; // false

echo "<br>";
echo "<br>";

echo "<h3> SOAL 2 </h3>";
$sungokong = new ape("kera sakti");
echo $sungokong->name; // "Auooo"
echo "<br>";
$sungokong->yell(); // "Auooo"
echo "<br>";
echo $sungokong->legs;

echo "<br>";
echo "<br>";

$kodok = new frog("buduk");
echo $kodok->name; // "Auooo"
echo "<br>";
$kodok->jump() ; // "hop hop"
echo "<br>";
echo $kodok->legs;

?>